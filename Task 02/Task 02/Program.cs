﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            var month = " ";
            var day = 0;

            Console.WriteLine("Please type in the month you were born");
            month = Console.ReadLine();

            Console.WriteLine("Please type in the day you were born");
            day = int.Parse(Console.ReadLine());

            Console.WriteLine($"You were born on {month}, {day}.");
            
           
        }
    }
}
